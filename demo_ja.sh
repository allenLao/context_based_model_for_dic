#!/usr/bin/env bash

dataDir=./toy

if [ ! -d "$dataDir" ]; then
    echo "there is no data, please put it into directory toy"
    exit 1
fi

./featurize.py ./toy/ch.txt ./toy/ch.voc 2 ./toy/ch.fea
./featurize.py ./toy/ch.txt ./toy/ch.voc 2 ./toy/ch.fea
java ParallelCosineSimilarity ./toy/en.fea ./toy/ch.fea ./toy/seed.pair ./toy/jscores.txt
