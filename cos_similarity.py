#!/usr/bin/env python
import codecs
import sys
import math
from operator import itemgetter
def cosine(src, tgt, dic):
    score = 0
    snorm = 0
    tnorm = 0
    for tgt_word in tgt.keys():
        tnorm += tgt[tgt_word] * tgt[tgt_word]

    for src_word in src.keys():
        if dic[src_word] in tgt.keys():
            score += src[src_word] * tgt[dic[src_word]]
        snorm += src[src_word] * src[src_word]

    return score/(math.sqrt(snorm) *  math.sqrt(tnorm)  + 1.0e-7)


def compute_cos_similarity(argv):
    src_input = codecs.open(argv[0], 'r', 'utf-8')
    tgt_input = codecs.open(argv[1], 'r', 'utf-8')
    seed_input = codecs.open(argv[2], 'r', 'utf-8') #src-tgt pairs
    writer = codecs.open(argv[3], 'w', 'utf-8')

    src_data = {}
    tgt_data = {}
    seed = {}

    for line in src_input:
        line = line.strip()
        terms = line.split()
        word = terms[0]
        feas = terms[1:]
        src_data[word] = {}
        for fea in feas:
            blks = fea.split(':')
            src_data[word][blks[0]] = float(blks[1])

    for line in tgt_input:
        line = line.strip()
        terms = line.split()
        word = terms[0]
        feas = terms[1:]
        tgt_data[word] = {}
        for fea in feas:
            blks = fea.split(':')
            tgt_data[word][blks[0]] = float(blks[1])

    for line in seed_input:
        line = line.strip()
        blks = line.split()
        seed[blks[0]] = blks[1]

    for src_word in src_data.keys():
        scores = {}
        for tgt_word in tgt_data.keys():
            scores[tgt_word] = cosine(src_data[src_word], tgt_data[tgt_word], seed)
           # print tgt_word, scores[tgt_word]

        scores = sorted(scores.iteritems(), key=itemgetter(1), reverse=True)
        bf = src_word + " "
        for (k,v)in scores:
            bf += k + ":" + str(v) + " "

        writer.write(bf.strip() + "\n")
        writer.flush()

    writer.flush()
    writer.close()

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'src-fea, tgt-fea, seed, output'
        sys.exit(0)

    compute_cos_similarity(sys.argv[1:])
