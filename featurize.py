#!/usr/bin/env python
import codecs
import sys

def featurize(argv):
    words = codecs.open(argv[1], 'r', 'utf-8')
    data = codecs.open(argv[0], 'r', 'utf-8')
    win_size = int(argv[2])
    writer = codecs.open(argv[3], 'w', 'utf-8')

    wordMap = {} #seed
    rewordMap = {}
    idx = 0
    featureMap = {} #key=word; val={},
    for line in words:
        line = line.strip()
        wordMap[line] = idx
        rewordMap[idx] = line
        idx += 1


    for line in data:
        line = line.strip() #remove the whitespace
        terms = line.split() #
        for c in range(0, len(terms)):
            term = terms[c]
            if not (term in wordMap):
                b = max(0, c-win_size)
                e = min(len(terms), c+win_size)
                if not featureMap.has_key(term):
                    featureMap[term] = {}

                for w in range(b, e):
                    if w != c:
                        cw = terms[w]
                        if wordMap.has_key(cw):
                            if featureMap[term].has_key(wordMap[cw]):
                                featureMap[term][wordMap[cw]] += 1
                            else:
                                featureMap[term][wordMap[cw]] = 1

    for key in featureMap.keys():
        bf = key + " "
        for k,v in featureMap[key].iteritems():
            bf += rewordMap[k] +":" + str(v)+ " "
        writer.write(bf.strip() + "\n")
        writer.flush()
    writer.flush()
    writer.close()

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'data, seed, wind-size, output'
        sys.exit(0)

    featurize(sys.argv[1:])
