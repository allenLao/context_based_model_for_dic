import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParallelCosineSimilarity {
	private ExecutorService exec;
	private int corePoolSize;
	private HashMap<String, HashMap<String, Double>> sourceData;
	private HashMap<String, HashMap<String, Double>> targetData;
    private HashMap<String, String> seedDic;
	private FileWriter writer;
	private Vector<String> translations;

	public ParallelCosineSimilarity(String source, String target, String seed,
			String output, int corePoolSize) throws IOException {
		this.corePoolSize = corePoolSize;
		exec = Executors.newFixedThreadPool(this.corePoolSize);
		compute(source, target, seed, output);
	}

	public ParallelCosineSimilarity(String source, String target, String seed,
			String output) throws IOException {
		this(source, target, seed, output, 100);
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 4) {
			System.err
					.println("Usage: <source> <target> <seed> <output> <thread>");
			System.exit(0);
		}
		String source = args[0];
		String target = args[1];
		String seed = args[2];
		String output = args[3];
        if(args.length == 4){
           ParallelCosineSimilarity pcs =new  ParallelCosineSimilarity(source, target, seed, output);
        }else if(args.length == 5){
           ParallelCosineSimilarity pcs = new ParallelCosineSimilarity(source, target, seed, output, Integer.parseInt(args[4]));
        }
		long begin = System.currentTimeMillis();
		System.out.println("it takes around: "
				+ (System.currentTimeMillis() - begin) / 1000 + " senconds");
	}

	class Dispatch implements Runnable {
        String srcWord;
		final int kbest = 10;

		Dispatch(String srcWord) {
            this.srcWord = srcWord;
		}

		public void run() {
			long begin = System.currentTimeMillis();
            HashMap<String, Double> srcMap = sourceData.get(srcWord);
			final int min = Math.min(kbest, targetData.size());
			ArrayList<Pair> quene = new ArrayList<Pair>(min);

            Iterator<String> iter = targetData.keySet().iterator();
            while(iter.hasNext()){
                String tgtWord = iter.next();
                HashMap<String, Double> tgtMap = targetData.get(tgtWord);
				double v = computeCosine(srcMap, tgtMap);
				if (quene.size() < min) {
					quene.add(new Pair(tgtWord, v));
					Collections.sort(quene, new Comparator<Pair>() {
						public int compare(Pair p1, Pair p2) {
							return Double.compare(p1.val, p2.val);
						}});
				} else {
					if (quene.get(0).val < v) {
						quene.remove(0);
						quene.add(new Pair(tgtWord, v));
						Collections.sort(quene, new Comparator<Pair>() {
							public int compare(Pair p1, Pair p2) {
								return Double.compare(p1.val, p2.val);
						}});
					}
				}
			}

			StringBuffer buffer = new StringBuffer();
			buffer.append(srcWord + " ");
			for (int k = quene.size(); k > 0; k--) {
				Pair p = quene.get(k - 1);
				buffer.append(p.key + " " + p.val + " ");
			}
			translations.add(buffer.toString().trim() + "\n");
		}
	}

    private double computeCosine(HashMap<String, Double> srcMap, HashMap<String, Double> tgtMap){
        double score = 0;
        double snorm = 0, tnorm = 0;
        Iterator<String> iter = srcMap.keySet().iterator();
        while(iter.hasNext()){
            String key = iter.next();
            if(tgtMap.containsKey(seedDic.get(key))){
                score += srcMap.get(key) * tgtMap.get(seedDic.get(key));
            }
            snorm += srcMap.get(key) * srcMap.get(key);
        }
        iter = tgtMap.keySet().iterator();
        while(iter.hasNext()){
            String key = iter.next();
            tnorm += tgtMap.get(key) * tgtMap.get(key);
        }

        return score/(Math.sqrt(snorm) * Math.sqrt(tnorm) + 1.0e-7);
    }
	private class Pair {
		String key;
		double val;

		Pair(String key, double val) {
			this.key = key;
			this.val = val;
		}
	}


    private HashMap<String,String> loadSeedFile(String fin) throws IOException{
        HashMap<String,String> seedmap = new HashMap<String, String>();
        BufferedReader reader = new BufferedReader(new FileReader(fin));
        String line;
        while(null != (line = reader.readLine())){
            line = line.trim();
            String[] blks = line.split(" ");
            if(!seedmap.containsKey(blks[0])){
                seedmap.put(blks[0], blks[1]);
            }
        }
        return seedmap;
    }

	public void compute(String source, String target, String seed, String output)
			throws IOException {
        seedDic = loadSeedFile(seed);
		sourceData = loadData(source);
		targetData = loadData(target);

		// translation matrix
		translations = new Vector<String>();
        Iterator<String> iter = sourceData.keySet().iterator();
        while(iter.hasNext()){
			exec.submit(new Thread(new Dispatch(iter.next())));
		}
		exec.shutdown();
		while (!exec.isTerminated()) {
			int active = Thread.activeCount();
			System.out.println("still running threads:" + active);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("ThreadPool terminted!");
		System.out.println("output result:");
		File file = new File(output);
		writer = new FileWriter(file.getAbsoluteFile(), false);
        System.out.println(translations.size());
		for (String trans : translations) {
			writer.write(trans);
			writer.flush();
		}
		writer.close();
	}

	private HashMap<String, HashMap<String, Double>> loadData(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
        HashMap<String, HashMap<String, Double>> map = new HashMap<String, HashMap<String, Double>>();
		String line;

		while ((line = reader.readLine()) != null) {
			if ((line = line.trim()).isEmpty())
				break;
			String[] blocks = line.split(" ");
            String word = blocks[0];
            if(!map.containsKey(word)){
                HashMap<String, Double> tmpMap = new HashMap<String, Double>();
                map.put(word, tmpMap);
            }
            for (int i = 1; i < blocks.length; i++){
                String[] blks = blocks[i].split(":");
                map.get(word).put(blks[0], Double.parseDouble(blks[1]));
            }
		}
		reader.close();
		return map;
	}
}
